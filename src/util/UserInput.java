package util;
import view.View;

import java.util.Scanner;

public class UserInput {

    public static int input(String msg){
        View.output(msg);
        @SuppressWarnings("resource")
        int num = new Scanner(System.in).nextInt();
        return num;
    }

    public static String inputStr(String msg){
        View.output(msg);
        @SuppressWarnings("resource")
        String str =new Scanner(System.in).next();
        return str;
    }
}
