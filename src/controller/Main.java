package controller;
import controller.Controller;

public class Main {

    public static void main(String[] args) throws InstantiationException, Exception {
        Controller controller=new Controller();
        controller.control();
    }
}