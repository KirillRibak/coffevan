package controller;

import model.coffeeFactory.GrainCoffeeMaiker;
import model.coffeeFactory.InstantCoffeeMaker;
import model.container.Loader;
import model.container.Van;
import model.logic.Calculation;
import model.logic.Search;
import model.logic.Sort;
import model.product_details.VarietyCoffee;
import util.Rnd;
import util.UserInput;
import view.View;

public class Controller {
    private static Controller instance = new Controller();

    public Controller(){};

    public void control(){
        Van van = Van.getVan();
        Loader loader = new Loader();
        van = makeVan(van, loader);
        chooseAction(van, loader);
    }
    public Van makeVan(Van van, Loader loader) {

        for (int i=0;i<=van.getLOAD_CAPACITY();i++){
            Rnd rnd=new Rnd();
            int fate= rnd.RandomNum(2);
            if (fate==0){
                loader.addCoffee(van,new GrainCoffeeMaiker().createCoffee());
            }else if (fate==1){
                loader.addCoffee(van,new GrainCoffeeMaiker().createCoffee());
            }else {
                loader.addCoffee(van,new InstantCoffeeMaker().createCoffee());
            }


        View.output(van.toString());
        }
        return (van);
    }
    public void chooseAction(Van van, Loader loader){
        boolean flag = true;
        while (flag){
            int choice = UserInput.input("Press 1,2,3 if you want to search by variety coffee (ARABIC,LIBERIC,ROBUSTO)"
                            + ",4 - sort by variety"
                            + ",8 - sort by weight"
                            +",9 - sort by price"
                            + ",5 - cost of all Coffee "
                            + ",6 - The the total weight products"
                            + ",5 - show the total speed of all machines "
                            + ",6 - The the total weight products"
                            + ",7 - out");
            if (choice == 7){
                flag = false;
            }
            else{
                switch(choice){

                    case 1: {String msg=UserInput.inputStr("name of Coffee");
                        Search.search(van, VarietyCoffee.ARABIC);
                        break;}
                    case 2: {String msg=UserInput.inputStr("name of Coffee");
                        Search.search(van, VarietyCoffee.LIBERIC);
                        break;}
                    case 3: {String msg=UserInput.inputStr("name of Coffee");
                        Search.search(van, VarietyCoffee.ROBUSTO);
                        break;}
                    case 4:{
                        Sort.sortByName(van);
                        break;}
                    case 5:{
                        View.output("Cost of all Coffee = " + Calculation.calculatePriceVan(van));
                        break;
                    }
                    case 6: {
                        View.output("The the total weight products  = " + Calculation.calculateTotalWeight(van));
                        break;
                    }
                    case 8:{
                        Sort.sortByWeigth(van);
                        break;}
                    case 9:{
                        Sort.sortByPrice(van);
                        break;}
                }

            }
        }
    }

}
