package model.container;

import model.product.Coffee;

import java.util.Arrays;

public class Van {

    private int LOAD_CAPACITY=100;

    private Coffee coffees[];

    private static Van van;

    private int mass;

    public Van(Coffee...coffees) {
        this.coffees = coffees;
        this.mass = 0;
    }

    public static Van getVan() {
        if (van==null){
            van = new Van();
        }
        return van;
    }

    public Van(int size)
    {
        this.coffees=new Coffee[size];
    }

    public int getCountOfComponents() {
        return coffees.length;
    }

    public Coffee getValueCoffee(int index)
    {
        if(index >= 0 && index<coffees.length )
        {
            return coffees[index];
        }
        throw new ArrayIndexOutOfBoundsException();

    }

    public void setValueCoffee(int index,Coffee coffee)
    {
        if (index>=0 && index < coffees.length )
        {
            coffees[index] = coffee;
        }else {
            throw new ArrayIndexOutOfBoundsException();
        }
    }


    public int getLOAD_CAPACITY() {
        return LOAD_CAPACITY;
    }

    public int getMass(){
        return mass;
    }

    public void updateMass(int n){
        mass+=n;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("Inside ypur van there are : \n");

        for (Coffee elem : coffees) {
            builder.append(elem).append("\n");

        }
        return builder + "coffee pakeges";
    }
}
