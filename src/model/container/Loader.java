package model.container;

import model.product.Coffee;

public class Loader {

    public Van addCoffee( Van coffees,Coffee coffee)
    {
        Van van = new Van(coffees.getCountOfComponents()+1);
        for(int i = 0; coffees.getCountOfComponents() < coffees.getLOAD_CAPACITY() &&
                i < coffees.getCountOfComponents();i++)
        {
            van.setValueCoffee(i, coffees.getValueCoffee(i));
        }
        van.setValueCoffee(coffees.getCountOfComponents(), coffee);
        van.updateMass(coffee.getWeight());
        return van;
    }

}
