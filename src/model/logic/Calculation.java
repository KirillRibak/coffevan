package model.logic;

import model.container.Van;

public class Calculation {

    public static double calculatePriceVan(Van van)
    {
        int price = 0;
        for(int i =0 ; i < van.getCountOfComponents();i++)
        {
            price += van.getValueCoffee(i).getPrice();
        }
        return price;
    }

    public static double calculateTotalWeight(Van van)
    {
        int weight = 0;
        for(int i =0 ; i < van.getCountOfComponents();i++)
        {
            weight += van.getValueCoffee(i).getWeight();
        }
        return weight;
    }

}
