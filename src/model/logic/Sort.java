package model.logic;

import model.container.Van;
import model.product.Coffee;

public class Sort {

    public static void sortByWeigth(Van van){
        int item;
        for (int i = 1; i < van.getCountOfComponents(); i++) {
            Coffee temp =van.getValueCoffee(i);
            item = i - 1;
            while (item >= 0 && van.getValueCoffee(item).getWeight()> temp.getWeight()) {
                van.setValueCoffee(item+1,van.getValueCoffee(item));
                van.setValueCoffee(item, temp);
                item--;
            }
        }
    }

    public static void sortByPrice(Van van){
        int item;
        for (int i = 1; i < van.getCountOfComponents(); i++) {
            Coffee temp =van.getValueCoffee(i);
            item = i - 1;
            while (item >= 0 && van.getValueCoffee(item).getPrice()> temp.getPrice()) {
                van.setValueCoffee(item+1,van.getValueCoffee(item));
                van.setValueCoffee(item, temp);
                item--;
            }
        }
    }

    public static void sortByName(Van van){
        int item;
        for (int i = 1; i < van.getCountOfComponents(); i++) {
            Coffee temp = van.getValueCoffee(i);
            item = i - 1;
            while (item >= 0 && van.getValueCoffee(item).getVariety().compareTo(temp.getVariety()) > 0) {
                van.setValueCoffee(item+1,van.getValueCoffee(item));
                van.setValueCoffee(item, temp);
                item--;
            }
        }
    }
}
