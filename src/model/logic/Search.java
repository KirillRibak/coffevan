package model.logic;

import model.container.Van;
import model.product_details.VarietyCoffee;

public class Search {

     public static String search(Van van, VarietyCoffee variety)
    {
        StringBuilder result = new StringBuilder("");
        for (int i =0 ; i < van.getCountOfComponents();i++)
        {
            if (van.getValueCoffee(i).getVariety().equals(variety))
            {
                result.append(van.getValueCoffee(i)).append("\n");
            }
        }
        return result + "";
    }

}
