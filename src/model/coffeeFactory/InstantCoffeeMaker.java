package model.coffeeFactory;

import model.product_details.Grinding;
import model.product_details.Packaging;
import model.product_details.VarietyCoffee;
import model.product.Coffee;
import model.product.InstantCoffee;
import util.Rnd;

public class InstantCoffeeMaker implements CoffeeMaker {
    private Rnd rnd = new Rnd();

    private int flag = rnd.RandomNum(5);

    public Coffee createCoffee() {
        if (flag == 0) {
            return new InstantCoffee(VarietyCoffee.ARABIC, 50, Packaging.JAR);
        } else if (flag == 1) {
            return new InstantCoffee(VarietyCoffee.LIBERIC, 70, Packaging.JAR);
        } else if (flag == 2)  {
            return new InstantCoffee(VarietyCoffee.ROBUSTO, 40, Packaging.JAR);
        }else if(flag == 3)  {
            return new InstantCoffee(VarietyCoffee.ROBUSTO, 20,Packaging.PACKET);
        }else if(flag == 4)  {
            return new InstantCoffee(VarietyCoffee.LIBERIC, 50,Packaging.PACKET);
        }else {
            return new InstantCoffee(VarietyCoffee.ARABIC, 30,Packaging.PACKET);
        }
    }
}

