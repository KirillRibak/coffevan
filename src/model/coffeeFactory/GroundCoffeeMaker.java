package model.coffeeFactory;

import model.product_details.Grinding;
import model.product_details.VarietyCoffee;
import model.product.Coffee;
import model.product.GroundCoffee;
import util.Rnd;

public class GroundCoffeeMaker implements CoffeeMaker {
    private Rnd rnd = new Rnd();

    private int flag = rnd.RandomNum(5);

    public Coffee createCoffee() {
        if (flag == 0) {
            return new GroundCoffee(VarietyCoffee.ARABIC, 90, Grinding.LARGE );
        } else if (flag == 1) {
            return new GroundCoffee(VarietyCoffee.LIBERIC, 100,Grinding.LARGE);
        } else if (flag == 2)  {
            return new GroundCoffee(VarietyCoffee.ROBUSTO, 60,Grinding.LARGE);
        }else if(flag == 3)  {
            return new GroundCoffee(VarietyCoffee.ROBUSTO, 40,Grinding.SMALL);
        }else if(flag == 4)  {
            return new GroundCoffee(VarietyCoffee.LIBERIC, 60,Grinding.SMALL);
        }else {
            return new GroundCoffee(VarietyCoffee.ARABIC, 50,Grinding.SMALL);
        }
    }
}
