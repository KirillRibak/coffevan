package model.coffeeFactory;

import model.product_details.VarietyCoffee;
import model.product.Coffee;
import util.Rnd;

public class GrainCoffeeMaiker implements CoffeeMaker {

    private Rnd rnd = new Rnd();

    private int flag = rnd.RandomNum(2);

    public Coffee createCoffee() {
        if (flag == 0) {
            return new Coffee(VarietyCoffee.ARABIC, 100);
        } else if (flag == 1) {
            return new Coffee(VarietyCoffee.LIBERIC, 120);
        } else  {
            return new Coffee(VarietyCoffee.ROBUSTO, 80);
        }
    }
}


