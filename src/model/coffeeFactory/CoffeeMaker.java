package model.coffeeFactory;

import model.product.Coffee;

public interface CoffeeMaker {

    public Coffee createCoffee();
}
