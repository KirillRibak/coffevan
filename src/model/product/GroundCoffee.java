package model.product;

import model.product_details.ConditionCoffee;
import model.product_details.Grinding;
import model.product_details.VarietyCoffee;

public class GroundCoffee extends Coffee {

    private Grinding grinding;

    public GroundCoffee(VarietyCoffee variety, int price, Grinding grinding) {
        super(variety, price);
        this.grinding = grinding;
        this.setConditional(ConditionCoffee.GROUND);
        this.setWeight(15);
    }
    public Grinding getGrinding() {
        return grinding;
    }

    public void setGrinding(Grinding grinding) {
        this.grinding = grinding;
    }

}
