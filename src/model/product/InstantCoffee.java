package model.product;

import model.product_details.ConditionCoffee;
import model.product_details.Packaging;
import model.product_details.VarietyCoffee;

public class InstantCoffee extends Coffee {

    private Packaging packaging;

    public InstantCoffee(VarietyCoffee variety, int price, Packaging packaging) {
        super(variety,price);
        this.packaging = packaging;
        this.setConditional(ConditionCoffee.INSTANT);
        this.setWeight(10);
    }

    public Packaging getPackaging() {
        return packaging;
    }

    public void setPackaging(Packaging packaging) {
        this.packaging = packaging;
    }
}
