package model.product;

import model.product_details.ConditionCoffee;
import model.product_details.VarietyCoffee;

public class Coffee {
    private VarietyCoffee variety;
    private int price;
    private ConditionCoffee conditional;
    private int weight;

    public Coffee(VarietyCoffee variety, int price) {
        this.variety = variety;
        this.price=price;
        this.conditional=ConditionCoffee.GRAIN;
        this.weight=20;
    }

    public int getWeight() {
        return weight;
    }

    public ConditionCoffee getConditional() {
        return conditional;
    }

    public void setConditional(ConditionCoffee conditional) {
        this.conditional = conditional;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public VarietyCoffee getVariety() {
        return variety;
    }

    public void setVariety(VarietyCoffee variety) {
        this.variety = variety;
    }
}
